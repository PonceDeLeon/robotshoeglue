# Robot Shoe Glue 
![(Traditional Chinese) Manual for the robot](
https://drive.google.com/file/
d/1EMrpbXCp1vfjnM1KX_t1z1hoAFtyA6fv/view?usp=sharing)

**Objective**: The objective of this project is to have 
the Intermate robot first find the contour points of the 
shoe using the attached Intel Realsense, then move the 
robot arm to apply glue to the shoe.

The project contains two parts:
1. **Image part**: Where we take the image from the camera
and apply image processing techniques to extract contour 
points. The plan is to work with sets of 30 points.

2. **Robot part**: Once we get the set of points, we need to
send the points to the robot and have the arm move. Since the 
contour points are in the camera coordinate's system, we need
a way to change between camera and robot coordinates.

## Image Part
We use the realsense library to capture the image from the 
attached realsense camera. 
First, we take the captured image and we crop it so that only the
conveyor is focused. 

The `Itri_Vision` file has some 
defined image processing techniques which allow us to get
an image with only the contour of the shoe.  The function
will also check for the largest contour of the image. The largest
contour  should be the one going around the shoe, and not some 
other noise in the image.

## Robot Part

Once we have extracted the set of points, we need to tell the robot arm
how to move. We want to send 30 points, and we want the robot to be as fast
as possible. There are two basic approaches:
1. Send one point to the robot at a time and move. This approach does make 
the robot start to move almost immediately, however the motion of the robot
is choppy, there is a small stop after every point.
2. Send all the 30 points firt, then move. The motion on this approach is smooth,
except sending all the points requires too much time.

And this is the main issue right now.

### Issue
Essentially, the roadblock is that the procedure has to be both quick and smooth, and so far
it seems that only one is possible. I have tried resetting the connection and even using another
Ethernet cable, to no avail.
I timed the process, and the most time-consuming portion seems to be waiting for the response from the 
robot after we send each point. It takes around 500-900 ms to send one point.

We have to use a request-response model for communicating with the robot
because if we don't, the script running on the robot control pad and the local 
machine will not be synchronized, and so when we send coordinates from the pc to the 
robot, the control pad will not read them. 
The pc sends the points way too quickly for the robot to process and store all of them,
and even if we add a delay between sending each point the problem persists. 

It may be the case that finding just the right delay solves the issue but I haven't found it and it seems
like an error-prone approach.
A possible solution might be to use an SDK to load our program onto the robot and avoid
the delay, but this remains to be tested.

To get started with the robot, we first have to connect to it.

## Connecting to the Robot
First, you will need to connect an Ethernet cable to the port on the backside of the bottom door.
There is a plastic covering on the top right corner of the back side which has a port underneath.
Then you have to set the IP address of your computer and the robot, as is laid out in the manual linked
in the first part of this document. We have to turn our firewall off and allow the 
same port on the control pad and pc in the network settings on Windows.

