#include "stdafx.h"
#include "RS232.h"

HANDLE hComm;

//HANDLE RS232_open(const char *port)
HANDLE RS232_open()
{
	DCB dcb;
	TCHAR *pccomport=TEXT("COM3");
	hComm = CreateFile(pccomport,
					   GENERIC_READ | GENERIC_WRITE,
					   0,
					   NULL,
					   OPEN_EXISTING,
					   0,
					   0);
	if (hComm == INVALID_HANDLE_VALUE)//如果COM未開啟
	{
		cout << "\n\n Can not open RS232" << endl;
		cout <<endl;
		return 0;
	}
	else
	{
		cout << "\n\n RS232 opened " << endl;
		cout <<endl;
	}
	

	GetCommState(hComm,&dcb);
	dcb.BaudRate = CBR_38400;
	dcb.ByteSize = 8;
	dcb.Parity = NOPARITY;
	dcb.StopBits = ONESTOPBIT;

	if (!SetCommState(hComm,&dcb))
	{
		cout << "\n\n Setcom fail" << endl;
		cout <<endl;
        CloseHandle(hComm);
        return 0;
	}

	COMMTIMEOUTS time_out;
                 time_out.ReadIntervalTimeout = MAXDWORD;
                 time_out.ReadTotalTimeoutConstant=0;
                 time_out.ReadTotalTimeoutMultiplier=0;
                 time_out.WriteTotalTimeoutConstant=50;
                 time_out.WriteTotalTimeoutMultiplier=5;
	SetCommTimeouts(hComm,&time_out);
          //      printf("setup and open com OK\n");
				cout <<endl;

	return hComm;
}


void RS232_write(char* buf)
{
	unsigned char key;
	DWORD write_bytes;

//	printf(" To RS232: %s \n", buf);

        /* evaluate the len of the buf */
	int size = 0;
	while(buf[size] != 0)
	{
		size++;
	}/* End of while-loop */
        
     /* write the char to the RS232 channel */
	for(int i=0 ; i < size ; i++)
	{        
		key = buf[i];
		write_bytes = 1;
		WriteFile(hComm, &key,write_bytes, &write_bytes, NULL);
	}
	//cout << "\n Writing done" << endl;
}

void RS232_read()
{
	unsigned char ch[1];

	DWORD read_bytes;
	bool check=false;


	do
	{  //cout << "Waiting to read" << endl;
		// read the one character from RS232 
		read_bytes = 1;//表示一次顯示一個字元
		ReadFile(hComm, ch, read_bytes, &read_bytes, NULL);
		if(read_bytes!=0)
		{
		}//有自元的地方，是一個"1"bit，沒得字元的話就是"0"bit		
		//	cout <<  ch[0] << endl;
			//printf("%c",ch[0]); // %c表示字元


		if(check==false && read_bytes!=0  ) check=true;
		//if(read_bytes==0 && check==true ) 
		//{
		//		printf("\n");
		//}
	}while (ch[0] != 's' && ch[0] != 'S');

	PurgeComm(hComm, PURGE_RXCLEAR); //清除串列附上的緩衝區
}

void RS232_stop()
{
PurgeComm(hComm, PURGE_RXCLEAR); //清除串列附上的緩衝區
 CloseHandle(hComm);
}


