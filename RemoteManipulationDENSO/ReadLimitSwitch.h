#ifndef READLIMITSWITCH_H
#define READLIMITSWITCH_H

#using <System.dll>
#include <iostream>

using namespace std;
using namespace System;
using namespace System::IO::Ports;

int getLimitSwitchVal();


#endif 