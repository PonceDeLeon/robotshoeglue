﻿#include "Arm.h"
#include <chrono>
#include "ReadDataTxtArray.h"

#define P2PCOORD 4 
#define LINECOORD 5
#define TOOL 6
#define P2PSPEED 7
#define LINESPEED 8 
#define INPUT 9
#define OUTPUT 10
#define EXIT 11
Robot::RobotArm::RobotArm()
{
	isLocked = false;
}
bool Robot::RobotArm::waitUntilRobotInput()
{
	char* on = "1";
	// Repeatedly query the robot if input is 1
	while (1) {
		printf("\Trying to query input...\n");
		this->queryInput(INPUT);	
		szRecv[1] = '\0';
		printf("szRecv after query: %s\n", szRecv);	
		//check the received value of the command
		//if (strncmp(on, szRecv, strlen(on) == 0)) {
		if (atoi(szRecv) == 1){
			break;
		}
		Sleep(500);
	}
	return true;
}
// Establish a connection to mechanical arm
bool Robot::RobotArm::connectRobot() 
{
	char joint[100]="";	char c;	char FileNameTxt[50];
	// TCP/IP 建置
	WSADATA wsa = {0};
	WSAStartup(MAKEWORD(2,2),&wsa);
	//建置socket
	sockclient = socket(AF_INET,SOCK_STREAM,IPPROTO_TCP);
	struct sockaddr_in addr = {0};	addr.sin_family = AF_INET;	addr.sin_port = htons(3000);
	//struct sockaddr_in addr = {0};	addr.sin_family = AF_INET;	addr.sin_port = htons(10000);
	addr.sin_addr.S_un.S_addr = inet_addr("192.168.0.1");// IP address on robot
	printf("Connecting...\n");
	int iRetVal = connect(sockclient,(struct sockaddr*)&addr,sizeof(addr));
	
	if (SOCKET_ERROR == iRetVal) { 
		printf("Connection Failed, closing socket.\n"); 
		printf("連線失敗!");		
		closesocket(sockclient); 
	}
	else { printf("connected!\n"); }
	return (iRetVal == SOCKET_ERROR) ? false : true;
}
// send values to the robot so it knows how to interpret numbers
bool Robot::RobotArm::prepareRobot(int commandNo)
{
	//isLocked = true;
	// Then have another routine on the control pad which reads the input
	snprintf(joint, sizeof(joint), "%d", commandNo);
	this->sendData();
	return true;
}
bool Robot::RobotArm::sendData()
{
	char* jdenso = joint;
	printf("\tSendnig string: %s\n", jdenso);
	send(sockclient, jdenso, strlen(joint), 0);
	//int received = recv(sockclient, szRecv, sizeof(szRecv), 0);
	//szRecv[100] = {1};

	//std::cout << "\tServer received: " << received
	//	<<"bytes." <<std::endl;
	printf("\tData received: %s\n", szRecv);
	
	return true;
}
void Robot::RobotArm::sendIntCoords(int x, int y, int z, int rx, int ry, int rz) 
{
	isLocked = true;
	snprintf(joint, sizeof(joint), "%d,%d,%d,%d,%d,%d\n", x,y,z,rx,ry,rz);
	printf("joint after assigning: %s\n", joint);
	this->sendData();

}
bool Robot::RobotArm::MovP(double x, double y, double z, double rx, double ry, double rz)
{
	//snprintf(joint, sizeof(joint), "%d\n", isMoveSet);
	//this->sendData();
	//if (!isMoveSet) {
	//	printf("isMoveSet is false, returning from move function.\n");
	//	return false;
	//}
	//snprintf(joint, sizeof(joint), "%09.1f,%09.1f,%09.1f,%09.1f,%09.1f,%09.1f\n",
	snprintf(joint, sizeof(joint), "%f,%f,%f,%f,%f,%f\n",
		x, y, z, rx, ry, rz);
	this->sendData();
	printf("Robot arm moved.\n");
	return true;
}
bool Robot::RobotArm::MovL(double x, double y, double z, double rx, double ry, double rz)
{
	this->prepareRobot(LINECOORD);
	printf("Moving arm in straight line to %d, %d, %d, %d, %d, %d\n", x, y, z, rx, ry, rz);
	snprintf(joint, sizeof(joint), "%09.4f,%09.4f,%09.4f,%09.4f,%09.4f,%09.4f",
		x, y, z, rx, ry, rz);
	printf("Robot arm moved in straight line.\n");
	this->sendData();
	return true;
}
void Robot::RobotArm::adjustPoints(std::vector<cv::Point>& contour,
	const double& xshift,
	const double& yshift) {
	for (int i = 0; i < contour.size(); i++) {
		contour[i].x + xshift;
		contour[i].y + yshift;
	}
}
bool Robot::RobotArm::setTool(double x, double y, double z, double rx, double ry, double rz)
{
	snprintf(joint, sizeof(joint), "%09.4f,%09.4f,%09.4f,%09.4f,%09.4f,%09.4f",
		x, y, z, rx, ry, rz);	
	this->sendData();
	printf("Set the tool coordinates.\n");
	return true;
}
bool Robot::RobotArm::setP2PSpeed(double speed)
{
	this->prepareRobot(P2PSPEED);
	return true;
}
bool Robot::RobotArm::setLineSpeed(double speed)
{
	this->prepareRobot(LINESPEED);
	snprintf(joint, sizeof(joint), "%d", speed);
	this->sendData();
	return true;
}
// Check if input/output is in valid range
bool Robot::RobotArm::queryInput(const int On)
{
	snprintf(joint, sizeof(joint), "%d\n", INPUT);
	this->sendData();
	return true;
}
bool Robot::RobotArm::setOutput(const int On)
{
	if (On != 0 && On != 1) {
		std::cout << "Toggling output to invalid value, please ensure either 0 or 1." << std::endl;
		return false;
	}
	this->prepareRobot(OUTPUT);
	snprintf(joint, sizeof(joint), "%d\n", On);
	this->sendData();


	return true;
}
void Robot::RobotArm::circlePath(Robot::Position& pos, int idx) 
{
	int radius = 10;
	int angle = 12;
	double mod = 0.1;
	angle = idx * angle;

	pos.x += (radius * cos(angle) * mod);
	pos.y += (radius * cos(angle) * mod);

}
