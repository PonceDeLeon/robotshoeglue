
#include "ReadDataTxtArray.h"

double** ReadDataTxtArray( string FileNameTxt,int numCols )
{
	L:
	fstream fp;
	vector<double> numbers;
	double number;

	fp.open(FileNameTxt, ios::in | ios::binary);

	if(fp.is_open())
	{
		while(fp >> number)
		{
			numbers.push_back(number);
			fp.get();
		}
	}
	else
	{
		cout<<"Couldn't find text file"<< endl;
		goto L;
	}

	fp.close();

	//cout << "Numbers:\n";
	//for (int i=0; i < numbers.size(); i++) {
	//	cout << numbers[i] << '\n';
	//}

	//Copy vector to array[ ]
	double** arrayData = 0; //[100][100];

	int numRows = numbers.size()/numCols;
	arrayData = new double*[numRows];
		
	
	int k = 0;
	for(  int i = 0; i < numRows; i++) 
	{
		arrayData[i] = new double[numCols];
		for (  int j = 0; j < numCols; j++) 
		{
			arrayData[i][j] = numbers[k];
			k+=1;
		}
	}
	
	//double ArrayPtr = arrayData;

	return arrayData;
}