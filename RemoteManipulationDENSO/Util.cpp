#include "Util.h"
void cropImageConveyor(cv::Mat& image, double x_scale, double y_scale)
{
//	printf("starting x: %f, offset x: %f", dims.x / image.size().width, dims.width / image.size().width);
	int x = image.size().width * .31;
	int y = 0;

	int x_offset = image.size().width * 0.45;
	int y_offset = image.size().height;
	cv::Rect conveyor;
	
	conveyor.x = x;
	conveyor.y = y;
	conveyor.width = x_offset;
	conveyor.height = y_offset;
	
	if (conveyor.width > image.size().width) {
		std::cout << "ROI is too wide" << std::endl;
		conveyor.width = image.size().width - 1;
	}
	if (conveyor.height > image.size().height) {
		conveyor.height = image.size().height - 1;
	}
		

	std::cout << conveyor << std::endl;
	image = image(conveyor);
	std::cout << "image size" << image.size() << std::endl;
}
void drawLargestContour(cv::Mat& image) {

}
void projectContoursTo3D(
	RealSense& rs,
	std::vector<std::vector<pcl::PointXYZ>>& conts3d,
	std::vector<std::vector<cv::Point>>& contours,
	std::vector<cv::Vec4i>& hierarchy
	)
{
	/*
		Loop through all the contours.
		1. Set the point to the class.
		2. Project the point to 3d.
		3. Get the projected point and set them to contours array
		Project each point in the contours to 3d individually.
	*/
	int cont_idx = 0;
	for (auto& contour : contours) {
		int point_idx = 0;
		for (auto& point : contour) {
			// First have to set point, then calculate, then get again
			rs.set_2d_to_class((cv::Point2f)point);
			rs.project_to_3d();
			rs.get_3d_from_class(conts3d[cont_idx][point_idx]);
		}
	}	
}

