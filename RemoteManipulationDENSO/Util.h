#pragma once
#ifndef UTIL_H
#define UTIL_H
#include "opencv2/opencv.hpp"
#include "realsense.h"
#include "modules/ITRI_Vision/ITRI_Vision.h"

// crop the image to include only the sole on conveyor
void cropImageConveyor(cv::Mat&, double x_scale=0.31, double y_scale=1.0);

void projectContoursTo3D(
	RealSense& rs,
	std::vector<std::vector<pcl::PointXYZ>>& conts3d,
	std::vector<std::vector<cv::Point>>& contours,
	std::vector<cv::Vec4i>& hierarchy);
std::vector<cv::Point> getLargestContour(const std::vector<std::vector<cv::Point>>& contours);
void drawLargestContour(cv::Mat& image);
void handleImage(ITRI_Vision_worker&);

#endif
