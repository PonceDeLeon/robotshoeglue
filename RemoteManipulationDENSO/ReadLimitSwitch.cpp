﻿
#include "ReadLimitSwitch.h"


int SwtchVal;
int LimitSwitchVal;

ref class PortDataReceived
{
public:
	static void MainSwich()
    {
        SerialPort^ mySerialPort = gcnew SerialPort("COM15");

        mySerialPort->BaudRate = 9600;
        mySerialPort->Parity = Parity::None;
        mySerialPort->StopBits = StopBits::One;
        mySerialPort->DataBits = 8;
        mySerialPort->Handshake = Handshake::None;
		mySerialPort->Open();

		mySerialPort->DataReceived += gcnew SerialDataReceivedEventHandler(DataReceivedHandler); //會被略過!?			
        //mySerialPort->Open();

        //Console::WriteLine();
        //Console::WriteLine();
		cout << "LimitSwich Signal = " << LimitSwitchVal << endl;
        //Console::ReadKey();
		//cout << SwtchVal << endl;
		mySerialPort->Close();
    } 
	
public:
//private:
    static void DataReceivedHandler( Object^ sender, SerialDataReceivedEventArgs^ e)
    {
        SerialPort^ sp = (SerialPort^)sender;
        String^ indata = sp->ReadExisting();
        __int64 LimitSwitchVal =  System::Int64::Parse(indata);
		//cout << LimitSwitchVal << endl;
		SwtchVal = LimitSwitchVal;	
		
		
    }
};

//void MainSwich()
//{
   // PortDataReceived::MainSwich();
	//return 0;
//}

int getLimitSwitchVal()
{
	PortDataReceived::MainSwich();
	//cout << LimitSwitchVal << endl;
	//cout << SwtchVal << endl;
	return SwtchVal; 	
}