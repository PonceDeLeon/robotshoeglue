#ifndef ARM_H
#define ARM_H

#include<iostream>
#include <winsock.h>
#include <conio.h>
#include <string.h>
#include <queue>
#include <windows.h>
#include "opencv2/opencv.hpp"
// Coords easier to manage if all a struct
namespace Robot {
	typedef struct Position {
		int x, y, z, rx, ry, rz;
	};

	class RobotArm {
	public:
		// Maybe a more robust way of doing the commands is to use
		// a queue of commands.
		RobotArm();
		bool connectRobot();

		// Move to a point (MOV)
		bool MovP(double, double, double, double, double, double);

		// Move to point in a straight line (MVS)
		bool MovL(double, double, double, double, double, double);

		// Set the tool coordinates (CHANGETOOL_VAR)
		bool setTool(double, double, double, double, double, double);

		// These only changeable through hard-wired values on robot side?
		bool setP2PSpeed(double);	//  As a percent of total speed
		bool setLineSpeed(double);  //  As a percent of total speed

		// We have to use set IO ports, so we can only
		// send the ON/OFF value
		bool queryInput(const int);

		// Tell the robot we're disconnecting
		bool disconnect();

		// Set a certain output on the robot, not sure if it works
		bool setOutput(const int);

		// debug to see if int values are the problem
		void sendIntCoords(int, int, int, int, int, int);

		// Tell the robot what parameter is being changed
		bool prepareRobot(int commandNo);

		// Repeatedly check the robot to see if the input is 1
		bool  waitUntilRobotInput();

		// Given the points in camera space, adjust the x, y to compensate for 
		//conveyor movement
		void adjustPoints(std::vector<cv::Point>&, const double&, const double&);

		// Test the robot motion by moving along circle
		void circlePath(Robot::Position&, int );

	private:
		//Send data to robot without waiting for response
		bool sendData();
		long unsigned int time;
		bool isMoveSet = false;
		bool isLocked; // ensure next command can't be sent early
		SOCKET sockclient;
		char joint[100];
		CHAR szRecv[100]; // store the data received from the robot
	};
}
#endif