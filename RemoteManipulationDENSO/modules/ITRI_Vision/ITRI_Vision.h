#pragma once
//@Flie   ITRI_Vision.h
//@Brief  Vision work is define here
//@Author Lance TSAI
//@Date   April. 26, 2018

#ifndef ITRI_Vision
#define ITRI_Vision

#include <iostream>
#include <opencv2/opencv.hpp>
#include <boost/shared_ptr.hpp>

#include <pcl/point_cloud.h>
#include <pcl/io/pcd_io.h>
#include <pcl/console/print.h>

class ITRI_Vision_worker
{
public:
	typedef boost::shared_ptr< ITRI_Vision_worker > Ptr;

public:
	std::vector<std::vector<cv::Point>> contours; // Vector for storing contour
	std::vector<cv::Vec4i> hierarchy;
	int largest_area = 0; // return this value
	int largest_contour_index = 0;
	

	std::vector<std::vector<cv::Point>> getContours();
	std::vector<cv::Vec4i> getHierarchy();
	int getLargestIndex();
	ITRI_Vision_worker();

	~ITRI_Vision_worker();

	//SURF Feature_Maching
	void Feature_Maching(cv::Mat &tempplate, cv::Mat &src, cv::Mat &out, cv::Point2f &center, float &angle);

	//Contour finding
	void Find_object_contour(cv::Mat &threshold_img, std::vector<cv::Point2f>& each_point_of_contour, cv::RotatedRect &bounding_rect,float &shrink_ratio);
	
	//Pre_precessing
	void Pre_precessing(cv::Mat &threshold, int &canny_value1, int &canny_value2);
	
	//Background remove
	cv::Mat Background_remove(cv::Mat & original_img);
	
	//Thin_Point
	void Thin_Point(std::vector<cv::Point>& countour_each_point_RCS);

	//Load the hand-eye calibration
	void LoadPlaneTransformationMat(Eigen::Matrix3f & _plane_transformation_mat);

	//Load the python point
	void LoadPythonPoint(Eigen::MatrixXf& countour_each_point_vector_ICS);

	//Transform the ICS to RCS
	void Trans_ICS_RCS(std::vector<cv::Point2f> &ICS_center, std::vector<cv::Point2f> &RCS_center, const Eigen::Matrix3f & transformation);

	//Drawing and show
	void Draw_and_show(cv::Mat &out, std::vector<cv::Point2f> &four_point, std::vector<cv::Point2f> &countour_each_point_ICS, cv::Point2f &center_ICS, std::vector<cv::Point2f> &inner_point, int &inner_mode);

	//Seperate three part
	void Seperate_contour(std::vector<cv::Point> &seperate_point_up, std::vector<cv::Point> &seperate_point_down, cv::RotatedRect &bounding_rect);

	//Find the head of the shoes
	void Find_object_head(std::vector<cv::Point> &countour_each_point_ICS, cv::Point2f &Shoose_head, cv::Point2f &center_ICS);

	// PCA Function declarations
	void drawAxis(cv::Mat& img, cv::Point &p, cv::Point &q, cv::Scalar &colour, double scale);

	double getOrientation(const std::vector<cv::Point> &pts, cv::Mat& img);

	void resize_contour(std::vector<cv::Point2f> &countour_in_ICS, cv::RotatedRect &bounding);

	void contourOffset(const std::vector<cv::Point>& src, std::vector<cv::Point>& dst, const cv::Point& offset);

	void scaleContour(const std::vector<cv::Point>& src, std::vector<cv::Point>& dst, float scale);

	void scaleContours(const std::vector<cv::Point>& src, std::vector<cv::Point>& dst, float scale);
};


#endif //Class_Header_