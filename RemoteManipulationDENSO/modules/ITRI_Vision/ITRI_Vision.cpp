#include "ITRI_Vision.h"
#include <math.h>
#include <iostream>
#include "opencv2/core.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/features2d.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/calib3d.hpp"
#include "C:/lib2017/opencv3/build/include/opencv2/xfeatures2d.hpp"

using namespace cv::xfeatures2d;

ITRI_Vision_worker::ITRI_Vision_worker()
{

}

ITRI_Vision_worker::~ITRI_Vision_worker()
{

}

void ITRI_Vision_worker::Feature_Maching(cv::Mat &tempplate, cv::Mat &src, cv::Mat &out, cv::Point2f &center, float &angle)
{
	cv::cvtColor(tempplate, tempplate, cv::COLOR_BGR2GRAY);
	//cv::cvtColor(src, src, cv::COLOR_BGR2GRAY);
	cv::Mat img_object = tempplate.clone();  //template
	cv::Mat img_scene  = src.clone();  //src
	cv::namedWindow("tem", cv::WINDOW_NORMAL);
	cv::namedWindow("src", cv::WINDOW_NORMAL);
	cv::imshow("tem", img_object);
	cv::imshow("src", img_scene);

	//-- Step 1: Detect the keypoints and extract descriptors using SURF
	int minHessian = 50;
	cv::Ptr<SURF> detector = SURF::create(minHessian);
	std::vector<cv::KeyPoint> keypoints_object, keypoints_scene;
	cv::Mat descriptors_object, descriptors_scene;
	detector->detectAndCompute(img_object, cv::Mat(), keypoints_object, descriptors_object);
	detector->detectAndCompute(img_scene, cv::Mat(), keypoints_scene, descriptors_scene);

	//-- Step 2: Matching descriptor vectors using FLANN matcher
	cv::FlannBasedMatcher matcher;
	std::vector<cv::DMatch > matches;
	matcher.match(descriptors_object, descriptors_scene, matches);
	double max_dist = 0; double min_dist = 100;
	//-- Quick calculation of max and min distances between keypoints
	for (int i = 0; i < descriptors_object.rows; i++)
	{
		double dist = matches[i].distance;
		if (dist < min_dist) min_dist = dist;
		if (dist > max_dist) max_dist = dist;
	}
	
	std::vector< cv::DMatch > good_matches;
	for (int i = 0; i < descriptors_object.rows; i++)
	{
		if (matches[i].distance < 3 * min_dist)
		{
			good_matches.push_back(matches[i]);
		}
	}
	std::cout << "Good maching points :  " << good_matches.size() << std::endl;
	std::vector<cv::Point2f> obj;
	std::vector<cv::Point2f> scene;
	for (size_t i = 0; i < good_matches.size(); i++)
	{
		//-- Get the keypoints from the good matches
		obj.push_back(keypoints_object[good_matches[i].queryIdx].pt);
		scene.push_back(keypoints_scene[good_matches[i].trainIdx].pt);
	}

	cv::Mat H = cv::findHomography(obj, scene, cv::RANSAC);
	//std::cout << "Homography :  " << H << std::endl;
	//-- Get the corners from the image_1 ( the object to be "detected" )
	std::vector<cv::Point2f> obj_corners(4);
	obj_corners[0] = cvPoint(0, 0); obj_corners[1] = cvPoint(img_object.cols, 0);
	obj_corners[2] = cvPoint(img_object.cols, img_object.rows); obj_corners[3] = cvPoint(0, img_object.rows);

	std::vector<cv::Point2f> scene_corners(4);
	perspectiveTransform(obj_corners, scene_corners, H);

	cvtColor(img_scene, img_scene, CV_GRAY2BGR);
	line(img_scene, scene_corners[0], scene_corners[1], cv::Scalar(0, 255, 0), 4);
	line(img_scene, scene_corners[1], scene_corners[2], cv::Scalar(0, 255, 0), 4);
	line(img_scene, scene_corners[2], scene_corners[3], cv::Scalar(0, 255, 0), 4);
	line(img_scene, scene_corners[3], scene_corners[0], cv::Scalar(0, 255, 0), 4);

	cv::Point2f center_point((scene_corners[1] + scene_corners[3]) / 2);
	cv::circle(img_scene, center_point, 10, cv::Scalar(0, 0, 255), 3);
	float angle_thistime = std::atan2(-(scene_corners[2].y - scene_corners[3].y), (scene_corners[2].x - scene_corners[3].x))*180.0 / 3.14;
	std::cout << "Center : " << center_point << std::endl;
	std::cout << "Angle  : " << angle_thistime << std::endl;
	cv::resize(img_scene, img_scene, cv::Size(640, 480));
	std::stringstream ss;
	ss << angle_thistime;
	std::string str = ss.str();
	cv::putText(img_scene, str, cv::Point(1,50), 0, 1, cv::Scalar(0, 255, 0), 5);
	cv::imshow("Object detection", img_scene);
	center = center_point;
	angle  = angle_thistime;
	out    = img_scene.clone();
}
// Need getters to draw contours around image
std::vector<std::vector<cv::Point>>
ITRI_Vision_worker::getContours() {
	return contours;
}
std::vector<cv::Vec4i>
ITRI_Vision_worker::getHierarchy() {
	return hierarchy;
}
int ITRI_Vision_worker::getLargestIndex() {
	return largest_contour_index;
}
void ITRI_Vision_worker::Find_object_contour(
	cv::Mat & threshold_img, 
	std::vector<cv::Point2f>& each_point_of_contour, 
	cv::RotatedRect &bounding_rect,
	float &shrink_ratio)
{
	//Find_countour
	std::vector<std::vector<cv::Point>> contours; // Vector for storing contour
	std::vector<cv::Vec4i> hierarchy;
	cv::Mat original = threshold_img.clone();
	cv::Mat smoothed = threshold_img.clone();
	cv::findContours(threshold_img, contours, hierarchy, CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE); // Find the contours in the image
    //CV_RETR_EXTERNAL：只取最外層的輪廓。
	//CV_RETR_LIST：取得所有輪廓，不建立階層(hierarchy)。
	//CV_RETR_CCOMP：取得所有輪廓，儲存成兩層的階層，首階層為物件外圍，第二階層為內部空心部分的輪廓，如果更內部有其餘物件，包含於首階層。
	//CV_RETR_TREE：取得所有輪廓，以全階層的方式儲存。

	// Iterate through each contour
	for (int i = 0; i < contours.size(); i++)
	{
		// Find the area of contour
		double a = contourArea(contours[i], false);
		// Store the index of largest contour
		if (a > largest_area)
		{
			largest_area = a;
			largest_contour_index = i;
		}
	}

	for (int i = 0; i < contours.size(); i++) // Iterate through each contour
	{
		if (i == largest_contour_index)
		{
			std::vector<cv::Point> dst;
			scaleContours(contours[i], dst, shrink_ratio);
			contours[i].clear();
			contours[i] = dst;
			bounding_rect = minAreaRect(contours[i]);
			for (int i = 0; i < contours[largest_contour_index].size(); i++)
			{
				cv::Point a(contours[largest_contour_index].at(i).x, contours[largest_contour_index].at(i).y);
				each_point_of_contour.push_back(a);
			}
		}
	}
}

void ITRI_Vision_worker::Pre_precessing(cv::Mat & threshold, int &canny_value1, int &canny_value2)
{
	//    Sharpen
	//////////////////////////////////////////////////////////
	//cv::Mat kernel(3, 3, CV_32F, cv::Scalar(0));
	//kernel.at<float>(1, 1) = 5.0;
	//kernel.at<float>(0, 1) = -1.0;
	//kernel.at<float>(1, 0) = -1.0;
	//kernel.at<float>(1, 2) = -1.0;
	//kernel.at<float>(2, 1) = -1.0;

	//threshold.create(threshold.size(), threshold.type());

	//cv::filter2D(threshold, threshold, threshold.depth(), kernel);
	//////////////////////////////////////////////////////////
	//Andy
	//cv::cvtColor(threshold, threshold, cv::COLOR_BGR2GRAY);
	cv::Mat threshold2;
	std::cout << "In Preprocessing" << std::endl;
	cv::GaussianBlur(threshold, threshold, cv::Size(0, 0), 3);
	cv::Canny(threshold, threshold, canny_value1, canny_value2, 5);
	//cv::Mat structure_element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(71, 71), cv::Point(-1, -1));
	cv::Mat structure_element2 = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(7, 7), cv::Point(-1, -1));
	cv::dilate(threshold, threshold, structure_element2);
	cv::imshow("Final preproc", threshold);

	//Lance
	////Threshold 
	//cv::Mat blured_img;
	//
	//cv::addWeighted(threshold, 1.2, blured_img, 0.2, 0, threshold);
	//cv::cvtColor(threshold, threshold, cv::COLOR_BGR2GRAY);
	//cv::threshold(threshold, threshold, threshold_value, 255, 1);

	//cv::adaptiveThreshold(threshold, threshold, 255, cv::ADAPTIVE_THRESH_GAUSSIAN_C
	//	, cv::THRESH_BINARY, 39, 0);
	//
	//for (int i = 0; i < 2; i++)
	//{
	//	cv::Mat structure_element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(20, 20), cv::Point(-1, -1));
	//	cv::morphologyEx(threshold, threshold, cv::MORPH_DILATE, structure_element);
	//}
	//for (int i = 0; i < 5; i++)
	//{
	//	cv::Mat structure_element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(5, 5), cv::Point(-1, -1));
	//	cv::morphologyEx(threshold, threshold, cv::MORPH_ERODE, structure_element);
	//}

	///*cv::namedWindow("Pre_processing", cv::WINDOW_NORMAL);
	//cv::imshow("Pre_processing", threshold);
	//cv::waitKey(0);*/
}

cv::Mat ITRI_Vision_worker::Background_remove(cv::Mat & original_img) {
	cv::Mat background_img = cv::imread(""); //current frame
	std::cout << "Background image: " << background_img.size() << std::endl;
	cv::Mat fgMaskMOG2; //fg mask fg mask generated by MOG2 method
	cv::Ptr<cv::BackgroundSubtractor> pMOG2; //MOG2 Background subtractor

	pMOG2 = cv::createBackgroundSubtractorMOG2(1, 100); //MOG2 approach

														//update the background model
	//pMOG2->apply(background_img, fgMaskMOG2);
	//update the path of the current frame
	pMOG2->apply(original_img, fgMaskMOG2);
	/*for (int i = 0; i < 1; i++)
	{
		cv::Mat structure_element = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(7, 7), cv::Point(-1, -1));
		cv::morphologyEx(fgMaskMOG2, fgMaskMOG2, cv::MORPH_CROSS, structure_element);
	}

	for (int i = 0; i < 3; i++)
	{
		cv::Mat structure_element = cv::getStructuringElement(cv::MORPH_ERODE, cv::Size(5, 5), cv::Point(-1, -1));
		cv::morphologyEx(fgMaskMOG2, fgMaskMOG2, cv::MORPH_ERODE, structure_element);
	}*/


	

	return fgMaskMOG2;
}


void ITRI_Vision_worker::Thin_Point(std::vector<cv::Point>& countour_each_point_RCS)
{
	std::vector<cv::Point> Thin_point;
	for (int i=0; i<countour_each_point_RCS.size(); i+=50)
	{
		Thin_point.push_back(countour_each_point_RCS.at(i));
	}
	countour_each_point_RCS = Thin_point;
}

void ITRI_Vision_worker::LoadPlaneTransformationMat(Eigen::Matrix3f & _plane_transformation_mat)
{
	std::string path;

	path = "C:/Users/Lance/Desktop/Shoes_Glue_FANUC/Shoose_glue/Shoes_glue/build/Calibration/plane_transformation_mat.txt";

	std::ifstream file(path);

	for (int row = 0; row < 3; row++)
	{
		std::string str;
		std::getline(file, str);
		std::stringstream ss(str);

		float value;

		for (int col = 0; col < 3; col++)
		{
			ss >> value;
			_plane_transformation_mat(row, col) = value;

			if (ss.peek() == ',')
			{
				ss.ignore();
			}
		}

	}

}

void ITRI_Vision_worker::LoadPythonPoint(Eigen::MatrixXf& countour_each_point_vector_ICS)
{
	std::string path;

	path = "C:/Users/Lance/Desktop/Shoes_Glue_FANUC/Shoose_glue/Shoes_glue/build/Points.txt";

	std::ifstream file(path);

	for (int row = 0; row < 100; row++)
	{
		std::string str;
		std::getline(file, str);
		std::stringstream ss(str);

		float value;

		for (int col = 0; col < 4; col++)
		{
			ss >> value;
			countour_each_point_vector_ICS(row, col) = value;

			if (ss.peek() == ',')
			{
				ss.ignore();
			}
			int a = value;
		}
	}
}

void ITRI_Vision_worker::Trans_ICS_RCS(std::vector<cv::Point2f> &ICS_center, std::vector<cv::Point2f> &RCS_center, const Eigen::Matrix3f & transformation)
{
	for (int i = 0; i < ICS_center.size(); i++)
	{
		cv::Point2f center(ICS_center.at(i).x, ICS_center.at(i).y);
		Eigen::Vector3f object_ics(center.x, center.y, 1.f);
		Eigen::Vector3f object_rcs = transformation*object_ics;
		object_rcs /= object_rcs.z();
		cv::Point2f center_rcs(object_rcs.x(), object_rcs.y());
		RCS_center.push_back(center_rcs);
	}
}

void ITRI_Vision_worker::Draw_and_show(cv::Mat &out, std::vector<cv::Point2f> &four_point, std::vector<cv::Point2f> &countour_each_point_ICS, cv::Point2f &center_ICS, std::vector<cv::Point2f> &inner_point, int &inner_mode)
{
	cv::line(out, four_point.at(0), four_point.at(1), cv::Scalar(0, 255, 0), 4);
	cv::line(out, four_point.at(1), four_point.at(2), cv::Scalar(0, 255, 0), 4);
	cv::line(out, four_point.at(2), four_point.at(3), cv::Scalar(0, 255, 0), 4);
	cv::line(out, four_point.at(3), four_point.at(0), cv::Scalar(0, 255, 0), 4);

	for (int i = 0; i <= 3; i++)
	{
		cv::circle(out, four_point.at(i), 10, cv::Scalar(255, 0, 0), 3);
	}

	/*cv::Point2f center1((four_point.at(0).x + four_point.at(1).x) / 2, (four_point.at(0).y + four_point.at(1).y));
	cv::Point2f center2((four_point.at(2).x + four_point.at(3).x) / 2, (four_point.at(2).y + four_point.at(3).y));
	cv::Point2f center_ICS((center1.x + center2.x) / 2, (center1.y + center2.y)/2);*/
	cv::circle(out, center_ICS, 15, cv::Scalar(0, 0, 255), 3);

	int length = countour_each_point_ICS.size() - 1;
	for (int i = 0; i < countour_each_point_ICS.size(); i++)
	{
		cv::circle(out, countour_each_point_ICS.at(i), 0.3, cv::Scalar(255, 0, 255), 3);
		if(i!= length)
		cv::line(out, countour_each_point_ICS.at(i), countour_each_point_ICS.at(i+1), cv::Scalar(255, 0, 255), 3);
	}
	if (inner_mode == 1) {
		for (int ci = 0; ci < inner_point.size(); ci++)
		{
			if (ci > 0) {
				cv::line(out, inner_point.at(ci - 1), inner_point.at(ci), cv::Scalar(0, 200, 200), 2);
			}
			cv::circle(out, inner_point.at(ci), 10, cv::Scalar(255, 255, 0), 3);
		}
	}

}

void ITRI_Vision_worker::Seperate_contour(std::vector<cv::Point> &seperate_point_up, std::vector<cv::Point> &seperate_point_down, cv::RotatedRect & bounding_rect)
{
	cv::Point2f vertices2f[4];
	bounding_rect.points(vertices2f);

	std::vector<cv::Point2f> four_point;
	for (int i = 0; i < 4; i++)
	{
		four_point.push_back(vertices2f[i]);
		//std::cout << "vertices2f: " << vertices2f[i] << std::endl;
	}

	std::sort(four_point.begin(), four_point.end(), [&](const cv::Point2f &first, const cv::Point2f &second)
	{
		if (((cvRound(second.x) - 500 < cvRound(first.x)) && (cvRound(first.x) <cvRound(second.x) + 500)))
		{
			return cvRound(first.y) < cvRound(second.y);
		}
		else
		{
			return cvRound(first.x) < cvRound(second.x);
		}
	});

	cv::Point2f center_up((four_point.at(0) + four_point.at(2)) / 2);
	cv::Point2f firt_point = (four_point.at(0) + center_up) / 2;
	cv::Point2f send_point = (center_up + four_point.at(2)) / 2;

	cv::Point2f center_down((four_point.at(1) + four_point.at(3)) / 2);
	cv::Point2f third_point  = (four_point.at(1) + center_down) / 2;
	cv::Point2f fourth_point = (center_down + four_point.at(3)) / 2;

	seperate_point_up.push_back(firt_point);
	seperate_point_up.push_back(send_point);
	seperate_point_down.push_back(third_point);
	seperate_point_down.push_back(fourth_point);
}

void ITRI_Vision_worker::Find_object_head(
	std::vector<cv::Point>& countour_each_point_ICS, 
	cv::Point2f & Shoose_head, 
	cv::Point2f &center_ICS)
{
	float largest_distance = 0.0;
	int   largest_distace_index = 0;

	for (int i = 0; i < countour_each_point_ICS.size(); i++)
	{
		// Find the most long distance.
		float distance = sqrt(pow(countour_each_point_ICS.at(i).x - center_ICS.x, 2) + pow(countour_each_point_ICS.at(i).y - center_ICS.y, 2));
		// Store the index of largest contour
		if (distance > largest_distance)
		{
			largest_distance = distance;
			largest_distace_index = i;
		}
	}

	Shoose_head = countour_each_point_ICS.at(largest_distace_index);
}

void ITRI_Vision_worker::drawAxis(cv::Mat& img, cv::Point &p, cv::Point &q, cv::Scalar &colour, double scale)
{
	double angle;
	double hypotenuse;
	angle = atan2((double)p.y - q.y, (double)p.x - q.x); // angle in radians
	hypotenuse = sqrt((double)(p.y - q.y) * (p.y - q.y) + (p.x - q.x) * (p.x - q.x));
	//    double degrees = angle * 180 / CV_PI; // convert radians to degrees (0-180 range)
	//    cout << "Degrees: " << abs(degrees - 180) << endl; // angle in 0-360 degrees range
	// Here we lengthen the arrow by a factor of scale
	q.x = (int)(p.x - scale * hypotenuse * cos(angle));
	q.y = (int)(p.y - scale * hypotenuse * sin(angle));
	line(img, p, q, colour, 1, CV_AA);
	// create the arrow hooks
	p.x = (int)(q.x + 9 * cos(angle + CV_PI / 4));
	p.y = (int)(q.y + 9 * sin(angle + CV_PI / 4));
	line(img, p, q, colour, 1, CV_AA);
	p.x = (int)(q.x + 9 * cos(angle - CV_PI / 4));
	p.y = (int)(q.y + 9 * sin(angle - CV_PI / 4));
	line(img, p, q, colour, 1, CV_AA);
}

double ITRI_Vision_worker::getOrientation(const std::vector<cv::Point> &pts, cv::Mat& img)
{
	//Construct a buffer used by the pca analysis
	int sz = static_cast<int>(pts.size());
	cv::Mat data_pts = cv::Mat(sz, 2, CV_64FC1);
	for (int i = 0; i < data_pts.rows; ++i)
	{
		data_pts.at<double>(i, 0) = pts[i].x;
		data_pts.at<double>(i, 1) = pts[i].y;
	}
	//Perform PCA analysis
	cv::PCA pca_analysis(data_pts, cv::Mat(), CV_PCA_DATA_AS_ROW);
	//Store the center of the object
	cv::Point cntr = cv::Point(static_cast<int>(pca_analysis.mean.at<double>(0, 0)),
		static_cast<int>(pca_analysis.mean.at<double>(0, 1)));
	//Store the eigenvalues and eigenvectors
	std::vector<cv::Point2d> eigen_vecs(2);
	std::vector<double> eigen_val(2);
	for (int i = 0; i < 2; ++i)
	{
		eigen_vecs[i] = cv::Point2d(pca_analysis.eigenvectors.at<double>(i, 0),
			pca_analysis.eigenvectors.at<double>(i, 1));
		eigen_val[i] = pca_analysis.eigenvalues.at<double>(0, i);
	}
	// Draw the principal components
	circle(img, cntr, 3, cv::Scalar(255, 0, 255), 2);
	cv::Point p1 = cntr + 0.02 * cv::Point(static_cast<int>(eigen_vecs[0].x * eigen_val[0]), static_cast<int>(eigen_vecs[0].y * eigen_val[0]));
	cv::Point p2 = cntr - 0.02 * cv::Point(static_cast<int>(eigen_vecs[1].x * eigen_val[1]), static_cast<int>(eigen_vecs[1].y * eigen_val[1]));
	drawAxis(img, cntr, p1, cv::Scalar(0, 255, 0), 1.0);
	drawAxis(img, cntr, p2, cv::Scalar(255, 255, 0), 5.0);
	double angle = atan2(eigen_vecs[0].y, eigen_vecs[0].x); // orientation in radians
	return angle;
}

void ITRI_Vision_worker::resize_contour(std::vector<cv::Point2f> &countour_in_ICS, cv::RotatedRect &bounding)
{
	//cv::Point2f shoes_center = bounding.center;

	//// 移動速度
	//float speed = 0.0f;
	//float distance;

	//for (auto &each_pt : countour_in_ICS)
	//{
	//	cv::Point2f vector_of_pt;
	//	distance = powf((shoes_center.x - each_pt.x), 2) + powf((shoes_center.y - each_pt.y), 2);
	//	distance = sqrtf(distance);
	//	vector_of_pt.x = float(shoes_center.x - each_pt.x) / distance;
	//	vector_of_pt.y = float(shoes_center.y - each_pt.y) / distance;
	//	vector_of_pt *= (distance == 0.0f ? 0.0f : speed / distance);
	//	each_pt += vector_of_pt;
	//}
}

void ITRI_Vision_worker::contourOffset(const std::vector<cv::Point>& src, std::vector<cv::Point>& dst, const cv::Point& offset)
{
	dst.clear();
	dst.resize(src.size());
	for (int j = 0; j < src.size(); j++)
		dst[j] = src[j] + offset;

}

void ITRI_Vision_worker::scaleContour(const std::vector<cv::Point>& src, std::vector<cv::Point>& dst, float scale)
{
	cv::Rect rct = cv::boundingRect(src);

	std::vector<cv::Point> dc_contour;
	cv::Point rct_offset(-rct.tl().x, -rct.tl().y);
	contourOffset(src, dc_contour, rct_offset);

	std::vector<cv::Point> dc_contour_scale(dc_contour.size());

	for (int i = 0; i < dc_contour.size(); i++)
		dc_contour_scale[i] = dc_contour[i] * scale;

	cv::Rect rct_scale = cv::boundingRect(dc_contour_scale);

	cv::Point offset((rct.width - rct_scale.width) / 2, (rct.height - rct_scale.height) / 2);
	offset -= rct_offset;
	dst.clear();
	dst.resize(dc_contour_scale.size());
	for (int i = 0; i < dc_contour_scale.size(); i++)
		dst[i] = dc_contour_scale[i] + offset;
}

void ITRI_Vision_worker::scaleContours(const std::vector<cv::Point>& src, std::vector<cv::Point>& dst, float scale)
{
	dst.clear();
	dst.resize(src.size());
	scaleContour(src, dst, scale);
}