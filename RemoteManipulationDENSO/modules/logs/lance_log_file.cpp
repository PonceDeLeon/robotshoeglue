//@Flie   my_log_file.cpp
//@Brief  Coding for the log file function.
//@Author Lance TSAI
//@Date   September. 13, 2017


#include "lance_log_file.h"
#include <iostream>
LoggFiles::LoggFiles()
{

}


LoggFiles::~LoggFiles()
{
	// DO NOTHING
}

void LoggFiles::StartLog()
{

	is_log_started_ = true;

	std::time_t rawtime;
	struct tm timeinfo;
	char buffer[80];

	std::time(&rawtime);
	localtime_s(&timeinfo, &rawtime);

	std::strftime(buffer, 80, "%Y%m%d_%H%M%S", &timeinfo);

	std::stringstream log_name;
	log_name << "./log_" << buffer << ".txt";

	log_dir_ = log_name.str();

	std::ofstream log_file = std::ofstream(log_dir_, std::ios::out | std::ios::trunc);

	if (log_file.is_open())
	{
		log_file << buffer << ": Log file open sucessfully.\n";
		log_file.close();
	}
	else
	{
		std::cout << "Unable to open log file\n";
		throw - 1;
	}

}

void LoggFiles::LogOutput(std::string _output)
{

	if (is_log_started_)
	{

		std::ofstream log_file = std::ofstream(log_dir_, std::ios::out | std::ios::app);

		if (log_file.is_open())
		{
			std::time_t rawtime;
			struct tm timeinfo;
			char buffer[80];

			std::time(&rawtime);
			localtime_s(&timeinfo, &rawtime);

			std::strftime(buffer, 80, "%Y%m%d_%H%M%S", &timeinfo);

			log_file << buffer << ": " << _output << std::endl;
			log_file.close();
		}
		else
		{
			std::cout << "Unable to open log file\n";
			throw - 1;
		}

	}

}

void LoggFiles::LogEnd()
{

	/*if (is_log_started_)
	{
	log_file_->close();
	}*/
}