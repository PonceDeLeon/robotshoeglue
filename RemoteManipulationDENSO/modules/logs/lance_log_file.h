//@Flie   my_log_file.h
//@Brief  Define log file function.
//@Author Lance TSAI
//@Date   September. 13, 2017

#ifndef LOG_FILES_HH_
#define LOG_FILES_HH_

//#include <iostream>
#include <fstream>
#include <boost/thread.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/filesystem.hpp>

class LoggFiles
{

public:
	typedef boost::shared_ptr< LoggFiles > Ptr;

public:
	LoggFiles();
	~LoggFiles();

	void StartLog();

	void LogOutput(std::string _output);

	void LogEnd();

protected:

	//boost::shared_ptr<std::ofstream> log_file_;

private:

	std::string  log_dir_;
	bool         is_log_started_ = false;

};


#endif  // LOG_FILES_H_