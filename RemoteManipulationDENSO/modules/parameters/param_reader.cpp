//@Flie   Param_reader.cpp
//@Brief  Coding about the param_reader function.
//@Author Lance TSAI
//@Date   September. 14, 2017

#include "param_reader.h"


my_ParamsReader::my_ParamsReader()
{
	initialize();
}


my_ParamsReader::~my_ParamsReader()
{
	// DO NOTHING
}

void my_ParamsReader::initialize()
{
	std::string File_name_be_read = "vision_relate_params/vision_relate_params.xml";
	std::string File_name1_be_read = "system_parameters/Robot_Params.xml";
	std::string File_name2_be_read = "robot_taugh_position/Robot_Taught_Positions.xml";
	std::string load_vision_relate_params_dir = params_dir_ + File_name_be_read;
	std::string load_robot_param_dir          = params_dir_ + File_name1_be_read;
	std::string load_robot_taugh_param_dir    = params_dir_ + File_name2_be_read;

	load(load_vision_relate_params_dir, VisionRelateParams);
	load(load_robot_param_dir,  RobotPARAMS);
	load(load_robot_taugh_param_dir, Robot_Taught_PARAMS);
	std::cout << "Initial Success" << '\n';
}

bool my_ParamsReader::load(std::string file, int type)
{
	cv::FileStorage fs(file, cv::FileStorage::READ);
	if (!fs.isOpened())
	{
		std::cerr << "Load file in dir: " << file << " failed!" << '\n';
		system("pause");
		throw std::exception("Load params failed");
	}

	switch (type)
	{
	case VisionRelateParams:
		fs["VisionRelateParams"] >> vision_relate_params;
		break;
	case RobotPARAMS:
		fs["RobotParams"] >> rboot_params;
		fs["IOParams"]    >> io_params;
		break;
	case Robot_Taught_PARAMS:
		fs["TaughtPositionParams"] >> robot_taught_params;
		fs["SafeBoxParams"] >> safe_box_params;
		break;

	default:
		std::cerr << "Wrong type name in ParamsReader::load ! " << std::endl;
		return false;
		break;
	}

	fs.release();

	return true;

}


static void operator>>(const cv::FileNode &_n, Vision_Relate_Params &_value)
{
	_n["shift_x"]    >> _value.shift_x;
	_n["shift_y"]    >> _value.shift_y;
	_n["over_press"] >> _value.over_press;
	_n["camera_id"] >> _value.camera_id;
	_n["head_angle"] >> _value.head_angle;
	_n["head_height"] >> _value.head_height;
	_n["middle_angle"] >> _value.middle_angle;
	_n["middle_height"] >> _value.middle_height;
	_n["back_angle"] >> _value.back_angle;
	_n["back_height"] >> _value.back_height;
	_n["start_x"] >> _value.start_x;
	_n["start_y"] >> _value.start_y;
	_n["start_width"] >> _value.start_width;
	_n["start_height"] >> _value.start_height;
	_n["threshold_value"] >> _value.threshold_value;
	_n["shrink_ratio"] >> _value.shrink_ratio;
	_n["h_factor"] >> _value.h_factor;
	_n["is_dark"] >> _value.is_dark;
	_n["max_glue_point"] >> _value.max_glue_point;
	_n["max_glue_center_point"] >> _value.max_glue_center_point;
	_n["canny_value1"] >> _value.canny_value1;
	_n["canny_value2"] >> _value.canny_value2;
	_n["inner_mode"] >> _value.inner_mode;
}

static void operator>>(const cv::FileNode &_n, Robot_PARAMS &_value)
{
	_n["slow_ptp"]  >> _value.slow_ptp;   
	_n["fast_ptp"]  >> _value.fast_ptp;		
	_n["slow_line"] >> _value.slow_line;		
	_n["fast_line"] >> _value.fast_line;			
}

static void operator>>(const cv::FileNode &_n, IO_Params &_value)
{
	_n["cusion"] >> _value.cusion;
	_n["platform"] >> _value.platform;
}

static void operator>>(const cv::FileNode &_n, Robot_Robot_Taught_PARAMS &_value)
{
	_value.Grab_Pos_ABC.x = _n["Grab_PosA"];
	_value.Grab_Pos_ABC.y = _n["Grab_PosB"];
	_value.Grab_Pos_ABC.z = _n["Grab_PosC"];

	_value.ini_home.v << _n["IniHome_PosX"], _n["IniHome_PosY"], _n["IniHome_PosZ"]
		, _value.Grab_Pos_ABC.x, _value.Grab_Pos_ABC.y, _value.Grab_Pos_ABC.z;

	_value.grab_pos.v << _n["Grab_PosX"], _n["Grab_PosY"], _n["Grab_Posz"]
		, _value.Grab_Pos_ABC.x, _value.Grab_Pos_ABC.y, _value.Grab_Pos_ABC.z;

	_value.stand_by_pos.v << _n["Stand_by_PosX"], _n["Stand_by_PosY"], _n["Stand_by_PosZ"]
		, _value.Grab_Pos_ABC.x, _value.Grab_Pos_ABC.y, _value.Grab_Pos_ABC.z;

	_value.RBP_shotABC.x = _n["RBP_shot_A"];
	_value.RBP_shotABC.y = _n["RBP_shot_B"];
	_value.RBP_shotABC.z = _n["RBP_shot_C"];
	_value.RBP_shot.v << _n["RBP_shot_X"], _n["RBP_shot_Y"], _n["RBP_shot_Z"]
		, _value.RBP_shotABC.x, _value.RBP_shotABC.y, _value.RBP_shotABC.z;

}

static void operator>>(const cv::FileNode &_n, Safe_Box_Params &_value)
{
	_value.Save_box_upper_limit.x =_n["SafeBox_UpperX"] ;
	_value.Save_box_upper_limit.y =_n["SafeBox_UpperY"] ;
	_value.Save_box_upper_limit.z =_n["SafeBox_UpperZ"] ;
        
	_value.Save_box_lower_limit.x = _n["SafeBox_LowerX"];
	_value.Save_box_lower_limit.y = _n["SafeBox_LowerY"];
	_value.Save_box_lower_limit.z = _n["SafeBox_LowerZ"];

	_value.Save_box_right_limit.x = _n["SafeBox_RightX"];
	_value.Save_box_right_limit.y = _n["SafeBox_RightY"];
	_value.Save_box_right_limit.z = _n["SafeBox_RightZ"];

	_value.Save_box_left_limit.x = _n["SafeBox_LeftX"];
	_value.Save_box_left_limit.y = _n["SafeBox_LeftY"];
	_value.Save_box_left_limit.z = _n["SafeBox_LeftZ"];

}