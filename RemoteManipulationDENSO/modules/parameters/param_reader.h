/**
*  @file   params_reader.h
*
*  @brief
*
*  @author Lance TSAI
*  @date   July 8, 2016
*/

#ifndef PARAMS_READER_HH_
#define PARAMS_READER_HH_

#include <opencv2/opencv.hpp>

//Boost lib
#include <boost/shared_ptr.hpp>

#include "param_structure.h"

//Using define to give the "load" function to clear which to load.
#define VisionRelateParams   0  
#define RobotPARAMS          1 
#define Robot_Taught_PARAMS  2 
/**
*  @class  ParamsReader
*  @brief
*/
class my_ParamsReader
{
public:
	typedef boost::shared_ptr< my_ParamsReader > Ptr;

	my_ParamsReader();
	~my_ParamsReader();
		
	bool load(std::string file, int type);

	Vision_Relate_Params        vision_relate_params;   //vision parameters.
	Robot_PARAMS			    rboot_params;           //robot speed , IP ...
	IO_Params				    io_params;              //robot I/O.
	Robot_Robot_Taught_PARAMS   robot_taught_params;    //robot teach posture.
	Safe_Box_Params             safe_box_params;         //some limit setting.


protected:

	void initialize();   

private:

	//bool is_debug_;      //If need can use.
	const std::string  params_dir_ = "./params/";
};

static void operator>>(const cv::FileNode &_n, Vision_Relate_Params  &_value);
static void operator>>(const cv::FileNode &_n, Robot_PARAMS  &_value);
static void operator>>(const cv::FileNode &_n, IO_Params  &_value);
static void operator>>(const cv::FileNode &_n, Robot_Robot_Taught_PARAMS  &_value);
static void operator>>(const cv::FileNode &_n, Safe_Box_Params  &_value);
#endif //PARAMS_READER_H_
