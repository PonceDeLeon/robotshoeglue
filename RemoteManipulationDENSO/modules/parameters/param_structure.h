//@Flie   Param_structure.h
//@Brief  Define the param to be read. 
//@Author Lance TSAI
//@Date   September. 14, 2017

#ifndef PARAMS_STRUCTURES_HH_
#define PARAMS_STRUCTURES_HH_

#include <iostream>

#include <OgreMatrix4.h>

//#include "C:/lib2017/robot_henry/itri_robot.h"
//#include "C:/lib2017/robot_henry/yaskawa_robot.h"
//#include "C:/lib2017/robot_henry/libpointcontainer.h"
//#include "C:/lib2017/robot_henry/robot_henry_helper.h"
#include "C:/lib2017/robot_henry/robot/BAS_MyStructure.h"


struct Vision_Relate_Params
{
	int  shift_x;
	int  shift_y;
	int  over_press;
	int  camera_id;
	int  head_angle;
	int  head_height;
	int  middle_angle;
	int  middle_height;
	int  back_angle;
	int  back_height;
	int  start_x;
	int  start_y;
	int  start_width;
	int  start_height;
	int  threshold_value;
	float shrink_ratio;
	int  h_factor;
	int  is_dark;
	int max_glue_point;
	int max_glue_center_point;
	int canny_value1;
	int canny_value2;
	int inner_mode;
};

struct Robot_PARAMS
{
	int  slow_ptp  ;
	int  fast_ptp  ;
	int  slow_line ;
	int  fast_line ;
};

struct IO_Params
{
	int  cusion;
	int  platform;
};

struct Robot_Robot_Taught_PARAMS
{
	RobotXYZABC   ini_home;       
	RobotXYZABC   grab_pos;
	RobotXYZABC   stand_by_pos;
	Ogre::Vector3 Grab_Pos_ABC;

	RobotXYZABC   RBP_shot;
	Ogre::Vector3 RBP_shotABC;
};

struct Safe_Box_Params
{
	Ogre::Vector3  Save_box_upper_limit;
	Ogre::Vector3  Save_box_lower_limit;
	Ogre::Vector3  Save_box_right_limit;
	Ogre::Vector3  Save_box_left_limit;
};

#endif //PARAMS_STRUCTURES_H_
