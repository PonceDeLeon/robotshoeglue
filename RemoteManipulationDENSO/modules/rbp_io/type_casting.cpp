/**
 *  @file   type_casting.cpp
 *
 *  @brief  
 *
 *  @author Chin-Chia Wu
 *  @date   Oct 07, 2013
 */

#include <vector>
#include <string>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include <OGRE/OgreMatrix4.h>
#include <OGRE/OgreQuaternion.h>

#include <Eigen/Geometry>

#include <opencv2/core/core.hpp>

#include "type_casting.h"

namespace rbp
{
namespace io
{


namespace internal
{
    template< typename ValT >
    struct cast_string_to_vector
    {
        std::vector< ValT > operator()( const std::string &_v )
        {
            std::vector< std::string > tokens = cast< std::vector< std::string > >( _v );

            std::vector< ValT > values;
            for (size_t k = 0; k < tokens.size(); ++k)
            {
                ValT val;
                try
                {   
                    val = boost::lexical_cast< ValT >( tokens[ k ] );
                    values.push_back( val );
                }
                catch ( const boost::bad_lexical_cast & )
                {
                    // unable to convert
                }
            }

            return values;
        }
    };

    struct cvt_to_float_cvmat 
    {
        cv::Mat operator()( const cv::Mat &_v )
        {
            cv::Mat float_mat;
            _v.convertTo( float_mat, CV_32FC1 );
            return float_mat;
        }
    };
}


/**
 *  @brief  type casting
 */
template<>
std::vector< std::string > cast< std::vector< std::string > >( const std::string &_v )
{
    std::string str( _v );
    boost::algorithm::trim( str );
    std::vector< std::string > tokens;
    boost::split( tokens, str, boost::is_any_of( ",: \t" ), boost::token_compress_on );
    // eliminate empty tokens
    tokens.erase( std::remove_if( tokens.begin(), tokens.end(), std::mem_fun_ref( &std::string::empty ) ), tokens.end() );
    
    return tokens;
}


template<>
std::vector< float > cast< std::vector< float > >( const std::string &_v )
{
    return internal::cast_string_to_vector< float >()( _v );
}


template<>
std::vector< double > cast< std::vector< double > >( const std::string &_v )
{
    return internal::cast_string_to_vector< double >()( _v );
}


template<>
Ogre::Matrix4 cast< Ogre::Matrix4 >( const Eigen::Matrix4f &_v )
{
    Ogre::Matrix4 m4x4( _v(0,0), _v(0,1), _v(0,2), _v(0,3),
                        _v(1,0), _v(1,1), _v(1,2), _v(1,3),
                        _v(2,0), _v(2,1), _v(2,2), _v(2,3),
                        _v(3,0), _v(3,1), _v(3,2), _v(3,3) );
    return m4x4;
}


template<>
Ogre::Matrix4 cast< Ogre::Matrix4 >( const Eigen::Affine3f &_v )
{
    const Eigen::Matrix4f &m = _v.matrix();
    Ogre::Matrix4 m4x4( m(0,0), m(0,1), m(0,2), m(0,3),
                        m(1,0), m(1,1), m(1,2), m(1,3),
                        m(2,0), m(2,1), m(2,2), m(2,3),
                        m(3,0), m(3,1), m(3,2), m(3,3) );
    return m4x4;
}



template<>
Eigen::Vector3f cast< Eigen::Vector3f >( const cv::Mat &_v )
{
    assert(_v.rows >= 3 || _v.cols >= 3);
    cv::Mat mf = internal::cvt_to_float_cvmat()( _v );

    return Eigen::Vector3f( mf.at< float >( 0 ), mf.at< float >( 1 ), mf.at< float >( 2 ) );
}


template<>
Eigen::Vector3f cast< Eigen::Vector3f >( const Ogre::Vector3 &_v )
{
    return Eigen::Vector3f( _v.x, _v.y, _v.z );
}


template<>
Eigen::Vector6f cast< Eigen::Vector6f >( const Ogre::Matrix4 &_v )
{
    Ogre::Matrix3 r3x3;
    _v.extract3x3Matrix( r3x3 );
    Ogre::Radian rx, ry, rz;    ///< roll (C), pitch (B), yaw (A)
    r3x3.ToEulerAnglesZYX( rz, ry, rx );
    Ogre::Vector3 t = _v.getTrans();

    // v(0--5): X, Y, Z, A(rz, yaw), B(ry, pitch), C(rx, roll)
    Eigen::Vector6f v;
    v << t[ 0 ], t[ 1 ], t[ 2 ], rz.valueDegrees(), ry.valueDegrees(), rx.valueDegrees();

    return v;
}


template<>
Eigen::Vector6f cast< Eigen::Vector6f >( const Eigen::Affine3f &_v )
{
    Ogre::Matrix4 m4x4 = cast< Ogre::Matrix4 >( _v.matrix() );
    
    return cast< Eigen::Vector6f >( m4x4 );
}


template<>
Eigen::Matrix3f cast< Eigen::Matrix3f >( const cv::Mat &_v )
{
    assert(_v.rows == 3 && _v.cols == 3);
    cv::Mat mf = internal::cvt_to_float_cvmat()( _v );
    cv::Mat_< float > &m = (cv::Mat_< float > &)mf;

    Eigen::Matrix3f m3x3;
    m3x3 << m(0, 0), m(0, 1), m(0, 2),
            m(1, 0), m(1, 1), m(1, 2),
            m(2, 0), m(2, 1), m(2, 2);
    return m3x3;
}


template<>
Eigen::Matrix3f cast< Eigen::Matrix3f >( const Ogre::Matrix3 &_v )
{
    Eigen::Matrix3f m3x3;
    m3x3 << _v[0][0], _v[0][1], _v[0][2],
            _v[1][0], _v[1][1], _v[1][2],
            _v[2][0], _v[2][1], _v[2][2];
    return m3x3;
}


template<>
Eigen::Matrix4f cast< Eigen::Matrix4f >( const Ogre::Matrix4 &_v )
{
    Eigen::Matrix4f m4x4;
    m4x4 << _v[0][0], _v[0][1], _v[0][2], _v[0][3],
            _v[1][0], _v[1][1], _v[1][2], _v[1][3],
            _v[2][0], _v[2][1], _v[2][2], _v[2][3],
            _v[3][0], _v[3][1], _v[3][2], _v[3][3];
    return m4x4;
}


template<>
Eigen::Affine3f cast< Eigen::Affine3f >( const cv::Mat &_v )
{
    assert(_v.rows == 4 && _v.cols == 4);
    cv::Mat mf = internal::cvt_to_float_cvmat()( _v );

    Eigen::Affine3f aff3;
    aff3.linear()       = cast< Eigen::Matrix3f >( mf( cv::Range( 0, 3 ), cv::Range( 0, 3 ) ) );
    aff3.translation()  = cast< Eigen::Vector3f >( mf.col( 3 ) );
    return aff3;
}


template<>
Eigen::Affine3f cast< Eigen::Affine3f >( const Ogre::Matrix4 &_v )
{
    Ogre::Matrix3 m3x3;
    _v.extract3x3Matrix( m3x3 );

    Eigen::Affine3f aff3;
    aff3.linear()       = cast< Eigen::Matrix3f >( m3x3 );
    aff3.translation()  = cast< Eigen::Vector3f >( _v.getTrans() ) ;
    return aff3;
}


template<>
Ogre::Vector3 cast< Ogre::Vector3 >( const cv::Mat &_v )
{
    assert(_v.size().area() == 3);
    cv::Mat mf = internal::cvt_to_float_cvmat()( _v );
    cv::Mat_< float > &v = (cv::Mat_< float > &)mf;

    return Ogre::Vector3( v( 0 ), v( 1 ), v( 2 ) );
}


template<>
Ogre::Matrix3 cast< Ogre::Matrix3 >( const cv::Mat &_v )
{
    assert(_v.rows == 3 && _v.cols == 3);
    cv::Mat mf = internal::cvt_to_float_cvmat()( _v );
    cv::Mat_< float > &v = (cv::Mat_< float > &)mf;

    Ogre::Matrix3 m3x3( v(0,0), v(0,1), v(0,2),
                        v(1,0), v(1,1), v(1,2),
                        v(2,0), v(2,1), v(2,2) );
    return m3x3;
}


template<>
Ogre::Matrix4 cast< Ogre::Matrix4 >( const cv::Mat &_v )
{
    assert(_v.rows == 4 && _v.cols == 4);
    cv::Mat mf = internal::cvt_to_float_cvmat()( _v );
    cv::Mat_< float > &v = (cv::Mat_< float > &)mf;

    Ogre::Matrix4 m4x4( v(0,0), v(0,1), v(0,2), v(0,3),
                        v(1,0), v(1,1), v(1,2), v(1,3),
                        v(2,0), v(2,1), v(2,2), v(2,3),
                        v(3,0), v(3,1), v(3,2), v(3,3) );
    return m4x4;
}


template<>
Ogre::Matrix4 cast< Ogre::Matrix4 >( const std::vector< double > &_v )
{
    assert(_v.size() >= 6);

    // A := Rz, B := Ry, C := Rx
    Ogre::Degree rx( _v[ 5 ] ), ry( _v[ 4 ] ), rz( _v[ 3 ] );
    Ogre::Matrix3 r3x3;
    r3x3.FromEulerAnglesZYX( rz, ry, rx );

    Ogre::Matrix4 tr4x4( r3x3 );
    tr4x4.setTrans( Ogre::Vector3( _v[ 0 ], _v[ 1 ], _v[ 2 ] ) );

    return tr4x4;
}


template<>
Ogre::Quaternion cast< Ogre::Quaternion >( const Eigen::Quaternionf &_v )
{
    return Ogre::Quaternion( _v.w(), _v.x(), _v.y(), _v.z() );
}


Eigen::Affine3f cvtAffine2dTo3dWithRotInXyPlane( const Eigen::Affine2f &_aff2 )
{
    /*
     *  aff2  ------>  aff3d
     *  
     *    R     u         R     0  u
     *     2x2  v          2x2  0  v
     *    0  0  1         0  0  1  0
     *                    0  0  0  1
     */
    Eigen::Affine3f aff3( Eigen::Matrix4f::Identity() );
    aff3.linear().topLeftCorner< 2, 2 >()   = _aff2.linear();  // top-left 2x2 block is the linear part
    aff3.translation().topRows< 2 >()       = _aff2.translation();

    return aff3;
}


Eigen::Affine2f cvtAffine3dTo2dWithRotInXyPlane( const Eigen::Affine3f &_aff3 )
{
    /*
     *  aff3d  ------>  aff2
     *  
     *    R     0  u      R     u
     *     2x2  0  v       2x2  v
     *    0  0  1  *      0  0  1
     *    0  0  0  1
     */
    Eigen::Affine2f aff2;
    aff2.linear()       = _aff3.linear().topLeftCorner< 2, 2 >();
    aff2.translation()  = _aff3.translation().topRows< 2 >();
    return aff2;
}


} //END-namespace io
} //END-namespace rbp
