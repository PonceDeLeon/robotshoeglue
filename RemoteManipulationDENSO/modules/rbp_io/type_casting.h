/**
 *  @file   type_casting.h
 *
 *  @brief  
 *
 *  @author Chin-Chia Wu
 *  @date   Oct 07, 2013
 */

#ifndef TYPE_CASTING_H
#define TYPE_CASTING_H

#include <Eigen/Geometry>


namespace Eigen
{
    typedef Matrix< float, 6, 1 >   Vector6f;
    typedef Matrix< double, 6, 1 >  Vector6d;
}


namespace rbp
{
namespace io
{

/**
 *  @brief  type casting
 */
template< typename DestT, typename SrcT >
DestT cast( const SrcT &_v );

/**
 *  @brief  Convert 2d affine to 3d affine (rotation in xy-plane)
 */
Eigen::Affine3f cvtAffine2dTo3dWithRotInXyPlane( const Eigen::Affine2f &_aff2 );

/**
 *  @brief  Convert 3d affine to 2d affine (rotation in xy-plane)
 */
Eigen::Affine2f cvtAffine3dTo2dWithRotInXyPlane( const Eigen::Affine3f &_aff3 );

} //END-namespace io
} //END-namespace rbp

#endif //TYPE_CASTING_H
