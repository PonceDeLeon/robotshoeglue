#include "realsense.h"
#include <streambuf>
#include <rs_advanced_mode.hpp>
//All of this is only for debugging you can delete it later
//#define DEBUGO
#ifdef DEBUGO
#define DEBUR(MSG) do{std::cout << "LS_RS2H: " << MSG << std::endl ;}while(false)
#else
#define DEBUR(MSG) do{}while(false)
#endif
//end of debugging stuff
const std::string PLAT_CAM = "Platform Camera";
rs2::context RealSense::ctx;
std::shared_mutex RealSense::_mapLock;
std::map<std::string, ViewPort> RealSense::_viewPorts;

//std::shared_ptr<rs2::context> RealSense::ctx;

// Constructor
RealSense::RealSense(rs2_serial & serial):
	_serialForObj(serial)
{
	if (serial.empty())
		std::cout << "MAJOR ERROR : no serial number provided" << std::endl;
}

// Destructor
RealSense::~RealSense()
{
	// Finalize
	finalize();
}

// Processing
void RealSense::run(cv::Mat &color_image, cv::Mat &depth_image, cv::Mat &infared_image, pcl::PointCloud<pcl::PointXYZRGB>::Ptr layers)
{
	// Update Data
	DEBUR("Run>Updating ...");
	update(_serialForObj/*, layers*/);

	// Draw Data
	//LS TODO: Not sure what this is needed for 
	draw();

	// Show Data
	DEBUR("Run>showing ...");
	show(color_image, depth_image, infared_image);
}

//rs2::frame RealSense::returnColorFrame(){
//    return color_frame;
//}

// Initialize
void RealSense::initialize(/*std::string &json_file*/)
{
	//ctx = std::make_shared<rs2::context>();
	//ctx.set_devices_changed_callback(changeInDevice);
	rs2_error* e = 0;
	cv::setUseOptimized(true);
	// Initialize Sensor
//	initializeSensor(serial_num, json_file);
	initializeSensor();
}
void RealSense::enableDevice(rs2::device dev) {
	rs2_serial serial = dev.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER);
	DEBUR("Enable devices getting lock");
	_mapLock.lock();
	DEBUR("Enable devices got lock");
	if (_viewPorts.find(serial) != _viewPorts.end()) {
		//Its already mapped
		_mapLock.unlock();
		return;
	}
	if (dev.get_info(RS2_CAMERA_INFO_NAME) == PLAT_CAM) {
		//We do not want Platform Cameras
		_mapLock.unlock();
		return;
	}
	//if (serial == "827312070893") {
	//	_mapLock.unlock();
	//	return;
	//}
	//Otherwise add camera to our map

	rs2::pipeline p;
	rs2::config c;
	std::cout << "LS_RS2: Enabling device: " << serial << std::endl;
	c.enable_device(serial);
	c.enable_stream(rs2_stream::RS2_STREAM_COLOR, DEF_WIDTH, DEF_HEIGHT, RS2_FORMAT_BGR8, DEF_FPS);
	c.enable_stream(rs2_stream::RS2_STREAM_DEPTH, DEF_WIDTH, DEF_HEIGHT, rs2_format::RS2_FORMAT_Z16,DEF_FPS);
	c.enable_stream(rs2_stream::RS2_STREAM_INFRARED, DEF_WIDTH, DEF_HEIGHT, rs2_format::RS2_FORMAT_Y8, DEF_FPS);
	rs2_error * e = 0;
	try {
		rs2::pipeline_profile profile = p.start(c);
			_viewPorts.emplace(serial, ViewPort{profile,p});
		std::cout << "LS_RS2:�@Emplaced device with serial :" << serial << std::endl;

	}
	catch (const rs2::error &e) {
		std::cout << "MAJOR PROBLEM: RealSense error calling" << e.get_failed_function() << "(" << e.get_failed_args() << "):\n"
			<< e.what() << endl;
		return;
	}
	_mapLock.unlock();
		DEBUR("LS_RS2:Letting go of lock");
}
// Initialize Sensor
inline void RealSense::initializeSensor(/*std::string &json_file*/)
{
	rs2_error* e = 0;

	// Create librealsense context for managing devices
	// No need for this
	//pipeline = rs2::context();
	//Emplace currently available devices
	DEBUR("Enabling devices");
	for (auto&& dev : ctx.query_devices()) {
		enableDevice(dev);
	}
	//auto profile = pipeline->get_active_profile();
	/* TODO: figure this out
		if (!json_file.empty())
		{
			std::ifstream t(json_file);
			std::string str((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
			rs400::advanced_mode dev = profile.get_device();
			dev.load_json(str);
		}
*/
}

// Finalize
void RealSense::finalize()
{
	// Stop Pipline
	//Stop all pipelines
	std::cout << "Locking mapLock for finalize" << std::endl;
	_mapLock.lock();
	auto vp = _viewPorts.begin();
	while (vp != _viewPorts.end()) {
		std::cout << "Stopping pipeline with serial " << vp->first << std::endl;
		vp->second.pipeline.stop();
		vp++;
	}
	_mapLock.unlock();
}
void RealSense::changeInDevice(rs2::event_information &evi) {
	std::cout << "LS_RS2: There has been a change in devices!" << std::endl;
	DEBUR("Getting that lock");
	_mapLock.lock();
	DEBUR("GOt dat lock");
	auto vp = _viewPorts.begin();
	while (vp != _viewPorts.end()) {
		if (evi.was_removed(vp->second.profile.get_device())) {
			std::cout << "LS_RS2: device " << vp->first << " disconnected" << std::endl;
			vp = _viewPorts.erase(vp);
		}
		else {
			vp++;
		}
	}
	_mapLock.unlock();
	for (auto &&dev : evi.get_new_devices()) {
		enableDevice(dev);
	}
}

// Update Data
void RealSense::update(rs2_serial & serial/*, pcl::PointCloud<pcl::PointXYZRGB>::Ptr &layers*/)
{
    // LUIS: im commenting all buupdate color to test the speed difference first
	// Update Frame(takes 15ms)
	//Aligned frame should be ready her
    // All Elements take 0ms
	// Update Color
	DEBUR("(Serial:"<<serial<<") Update>updateFrameset...");
	updateFrameset(_serialForObj);

	DEBUR("(Serial:"<<serial<<") Update>updateColor...");
	updateColor();

	// Update Depth
	DEBUR("(Serial:"<<serial<<") Update>updateDepth...");
	updateDepth();

	// Update Infrared
	DEBUR("(Serial:"<<serial<<") Update>updateInfraRed...");
	updateInfrared();

	// Update Point Cloud(takes ~400ms(lot slower ofc))
	DEBUR("(Serial:"<<serial<<") Update>updatePointCloud...");
	//updatePointCloud(layers);
}

// Update Frame
void RealSense::updateFrameset(rs2_serial & serial)
{
	_mapLock.lock();
	auto viewport = _viewPorts.find(serial);
	if (viewport == _viewPorts.end()) {
		_mapLock.unlock();
		std::cout << "LS_RS2 ERROR: The camera you are trying to update is not plugged\
			or has encountered a problem" <<std::endl;
		return;
	}
	else {
		if (viewport->second.pipeline.poll_for_frames(&frameset) == false) {
			std::cout << "LS_RS2 ERROR: We could not find frames for"<<
				"camera with serial : "<< viewport->first	<< std::endl;
			_mapLock.unlock();
			return;
		}
	}

	rs2::align align(rs2_stream::RS2_STREAM_COLOR);
	//rs2::frameset alignedFrameset = align.process(fs);
	aligned_frameset = align.process(frameset);
	if (!aligned_frameset)
		std::cout << "aligned_framset.size().failed" << std::endl;
	_mapLock.unlock();
	return;
}

// Update Color
inline void RealSense::updateColor()
{
	// Retrieve Color Frame
	color_frame = aligned_frameset.get_color_frame();

	// Retrive Frame Size
	color_width = color_frame.as<rs2::video_frame>().get_width();
	color_height = color_frame.as<rs2::video_frame>().get_height();
	printf("Got color frame of dimensions: %d,%d\n", color_width, color_height);
}

// Update Depth
inline void RealSense::updateDepth()
{
	// Retrieve Depth Flame
	depth_frame = aligned_frameset.get_depth_frame();

	// Retrive Frame Size
	depth_width = depth_frame.as<rs2::video_frame>().get_width();
	depth_height = depth_frame.as<rs2::video_frame>().get_height();
}


void RealSense::get_distance(float & x, float & y, float & distance)
{
	rs2::depth_frame dpt_frame = depth_frame.as<rs2::depth_frame>();
	float pixel_distance_in_meters = dpt_frame.get_distance(x, y);
	distance = pixel_distance_in_meters;
}


// Update Infrared
inline void RealSense::updateInfrared()
{
	// Retrieve Infrared Flame
	infrared_frame = frameset.first(rs2_stream::RS2_STREAM_INFRARED);

	// Retrive Frame Size
	infrared_width = infrared_frame.as<rs2::video_frame>().get_width();
	infrared_height = infrared_frame.as<rs2::video_frame>().get_height();
}

inline void RealSense::updatePointCloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr &layers)
{
	// Declare pointcloud object, for calculating pointclouds and texture mappings
	rs2::pointcloud pc;

	pc.map_to(color_frame);
	auto points = pc.calculate(depth_frame);

	pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);

	auto sp = points.get_profile().as<rs2::video_stream_profile>();
	
	// Config of PCL Cloud object
	cloud->width = static_cast<uint32_t>(sp.width());
	cloud->height = static_cast<uint32_t>(sp.height());
	cloud->is_dense = false;
	cloud->points.resize(points.size());

	auto tex_coords = points.get_texture_coordinates();
    //Vertices Structs are composed of 3 floats
	auto vertices = points.get_vertices();

    std::tuple<uint8_t, uint8_t, uint8_t> current_color;
	for (int i = 0; i < points.size(); ++i)
	{
        //From rs2 to pcl
		cloud->points[i].x = vertices[i].x;
		cloud->points[i].y = vertices[i].y;
		cloud->points[i].z = vertices[i].z;
      //  std::copy((float*)&vertices[i].x,(float*)&vertices[i+1],&cloud->points[i]);

        //TODO see if we can optimize this
		current_color = RGB_Texture(color_frame, tex_coords[i]);

		// Reversed order- 2-1-0 because of BGR model used in camera
		cloud->points[i].r = std::get<2>(current_color);
		cloud->points[i].g = std::get<1>(current_color);
		cloud->points[i].b = std::get<0>(current_color);
	}
	layers = cloud;
}

// Draw Data
void RealSense::draw()
{
	// Draw Color
	drawColor();

	// Draw Depth
	drawDepth();

	// Draw Infrared
	drawInfrared();

	// Draw Point Cloud
	//drawPointCloud();
}

// Draw Color
inline void RealSense::drawColor()
{
	// Create cv::Mat form Color Frame

	color_mat = cv::Mat(color_height, color_width, CV_8UC3, const_cast<void*>(color_frame.get_data()));
	std::cout << "Is color mat empty: " << color_mat.empty() << std::endl;
}

// Draw Depth
inline void RealSense::drawDepth()
{
	// Create cv::Mat form Depth Frame
	depth_mat = cv::Mat(depth_height, depth_width, CV_16SC1, const_cast<void*>(depth_frame.get_data()));
}

// Draw Infrared
inline void RealSense::drawInfrared()
{
	// Create cv::Mat form Infrared Frame
	infrared_mat = cv::Mat(infrared_height, infrared_width, CV_8UC1, const_cast<void*>(infrared_frame.get_data()));
}

// Draw Point Cloud
inline void RealSense::drawPointCloud()
{
	// Retrieve Vetrices
	const rs2::vertex* vertices = points.get_vertices();

	// Retrieve Coordinated Texture
	const rs2::texture_coordinate* texture_coordinates = points.get_texture_coordinates();

	// Create cv::Mat from Vertices and Texture
	vertices_mat = cv::Mat(depth_height, depth_width, CV_32FC3, cv::Vec3f::all(std::numeric_limits<float>::quiet_NaN()));
	texture_mat = cv::Mat(depth_height, depth_width, CV_8UC3, cv::Vec3b::all(0));

#pragma omp parallel for
	for (int32_t index = 0; index < points.size(); index++) 
	{
		if (vertices[index].z) 
		{
			// Set Vetices to cv::Mat
			const rs2::vertex vertex = vertices[index];
			vertices_mat.at<cv::Vec3f>(index) = cv::Vec3f(vertex.x, vertex.y, vertex.z);

			// Set Texture to cv::Mat
			const rs2::texture_coordinate texture_coordinate = texture_coordinates[index];
			const uint32_t x = static_cast<uint32_t>(texture_coordinate.u * static_cast<float>(color_width)); // [0.0, 1.0) -> [0, width)
			const uint32_t y = static_cast<uint32_t>(texture_coordinate.v * static_cast<float>(color_height)); // [0.0, 1.0) -> [0, height)
			if ((0 <= x) && (x < color_width) && (0 <= y) && (y < color_height)) {
				texture_mat.at<cv::Vec3b>(index) = color_mat.at<cv::Vec3b>(y, x);
			}
		}
	}
}

// Show Data
void RealSense::show(cv::Mat &color_image, cv::Mat &depth_image, cv::Mat &infared_image)
{
	// Show Color
	showColor(color_image);
	DEBUR("TESTING DAT COLOR image  : "<<color_image.empty());

	// Show Depth
	showDepth(depth_image);

	// Show Infrared
	showInfrared(infared_image);

	// Show Point Cloud
	showPointCloud();
}

// Show Color
inline void RealSense::showColor(cv::Mat &color_image)
{
	if (color_mat.empty()) {
		printf("\tThe color mat is empty!\n");
		return;
	}
	DEBUR("COlor mat testo : " << color_mat.empty());

	DEBUR("color imagae pre testo : " << color_image.empty());
	// Show Color Image
	color_image = color_mat.clone();
	DEBUR("color imagae post testo : " << color_image.empty());
	//cv::imshow( "Color", color_mat );
}

// Show Depth
inline void RealSense::showDepth(cv::Mat &depth_image)
{
	if (depth_mat.empty()) {
		return;
	}
	//depth_image = depth_mat.clone();

	////// Scaling
	cv::Mat scale_mat;
	////depth_mat.convertTo( scale_mat, CV_8U, -255.0 / 100, 255.0 ); // 0-10000 -> 255(white)-0(black)
	depth_mat.convertTo(scale_mat, CV_8U, -255.0 / 1000.0, 255.0); // 0-10000 -> 255(white)-0(black)

	//double min, max;
	//cv::minMaxLoc(depth_mat, &min, &max);
	//float scale = 255 / (max - min);
	//depth_mat.convertTo(scale_mat, CV_8UC1, scale, 255);
	//cv::applyColorMap(scale_mat, scale_mat, cv::COLORMAP_AUTUMN);

	depth_image = scale_mat.clone();
}

// Show Infrared
inline void RealSense::showInfrared(cv::Mat &infared_image)
{
	if (infrared_mat.empty()) {
		return;
	}

	// Show Infrared Image
	infared_image = infrared_mat.clone();
	//cv::imshow( "Infrared", infrared_mat );
}

// Show Point Cloud
inline void RealSense::showPointCloud()
{
	if (vertices_mat.empty()) {
		return;
	}

	if (texture_mat.empty()) {
		return;
	}
}


void RealSense::set_3d_to_class(pcl::PointXYZ & get_pcl_point)
{
	pcl_point_in_class = get_pcl_point;
}

void RealSense::get_2d_from_class(cv::Point2f & get_project_2d)
{
	get_project_2d = project_2d_in_class;
}

void RealSense::project_to_2d()
{
	float pixel[2]; // From point (in 2D)
	float point[3]; // From point (in 3D)
	
	point[0] = pcl_point_in_class.x ;
	point[1] = pcl_point_in_class.y ;
	point[2] = pcl_point_in_class.z ;

	rs2_intrinsics intr = color_frame.get_profile().as<rs2::video_stream_profile>().get_intrinsics();
	rs2_project_point_to_pixel(pixel, &intr, point);

	project_2d_in_class.x = pixel[0];
	project_2d_in_class.y = pixel[1];
}

// Enable Advanced Mode
void RealSense::enableAdvancedMode(int min, int max)
{
	// Retrieve Device
	bool exito = false;
	_mapLock.lock_shared();
	auto vp = _viewPorts.begin();
	while (vp != _viewPorts.end()) {
		if (vp->first == _serialForObj) {
			break;
		}
		vp++;
	}
	if (vp == _viewPorts.end())
		exito = true;

	_mapLock.unlock();
	if (exito) {
		std::cout << "LS_RS2 : Could not go into advanced mode" << std::endl;
		return;
	}

	rs2::device device = vp->second.profile.get_device();

	// Advanced Mode
	if (device.is<rs400::advanced_mode>()) 
	{
		// Enable Advanced Mode
		rs400::advanced_mode advanced_mode = device.as<rs400::advanced_mode>();
		if (!advanced_mode.is_enabled()) {
			advanced_mode.toggle_advanced_mode(true);
		}
		//std::cout << "debug\n";
		// e.g. Control Depth Table (Clamp Depth to Specified Range)
		STDepthTableControl depth_table_control = advanced_mode.get_depth_table();
		depth_table_control.depthClampMin = min; // Min Depth 1.0m = 1000
		depth_table_control.depthClampMax = max; // Max Depth 2.0m = 2000
		advanced_mode.set_depth_table(depth_table_control);

		// e.g. Apply Visual Preset (https://github.com/IntelRealSense/librealsense/wiki/D400-Series-Visual-Presets)
	}
}

void RealSense::set_depth_range(int min, int max)
{
	
}

std::tuple<uint8_t, uint8_t, uint8_t> RealSense::RGB_Texture(rs2::video_frame texture, rs2::texture_coordinate Texture_XY)
{
	// Get Width and Height coordinates of texture
	int width = texture.get_width();  // Frame width in pixels
	int height = texture.get_height(); // Frame height in pixels

									   // Normals to Texture Coordinates conversion
	int x_value = std::min(std::max(int(Texture_XY.u * width + .5f), 0), width - 1);
	int y_value = std::min(std::max(int(Texture_XY.v * height + .5f), 0), height - 1);

	
	int idx = x_value * texture.get_bytes_per_pixel() + y_value * texture.get_stride_in_bytes();
	const auto texture_data = reinterpret_cast<const uint8_t*>(texture.get_data());
	return std::tuple<uint8_t, uint8_t, uint8_t>(texture_data[idx], texture_data[idx + 1], texture_data[idx + 2]);
}
void RealSense::project_to_3d()
{
	float pixel[2];
	float point[3]; // From point (in 3D)
	pixel[0] = image_2d_in_class.x;
	pixel[1] = image_2d_in_class.y;
	float dist = aligned_frameset.get_depth_frame().get_distance(pixel[0], pixel[1]);

	rs2_intrinsics intr = color_frame.get_profile().as<rs2::video_stream_profile>().get_intrinsics();
	rs2_deproject_pixel_to_point(point, &intr, pixel, dist);
	camera_point_in_class.x = point[0];
	camera_point_in_class.y = point[1];
	camera_point_in_class.z = point[2];
}
void RealSense::set_2d_to_class(cv::Point2f & get_image_2d)
{
	image_2d_in_class = get_image_2d;
}
void RealSense::get_3d_from_class(pcl::PointXYZ & get_camera_point)
{
	camera_point_in_class.x *= 1000;
	camera_point_in_class.y *= 1000;
	camera_point_in_class.z *= 1000;
	DEBUR("Setting artifical get_camera_point");
	get_camera_point.x = 0;
	get_camera_point.y = 0;
	get_camera_point.z = 0;
	DEBUR("Setting get_camera_point");
	get_camera_point = camera_point_in_class;
	DEBUR("Done get_camera_point");
}

void RealSense::setSerial(rs2_serial & serialForObj) {
	this->_serialForObj = serialForObj;
}
bool RealSense::getInitStatus() {
	//Check if this serial is in the hashmap
	DEBUR("TRYING TO GET DAT LOCK FOR " << _serialForObj);
	_mapLock.lock_shared();
	DEBUR("Get init got dat lock");
	auto vp = _viewPorts.begin();
	while (vp != _viewPorts.end()) {
		DEBUR("Checking match for "<<vp->first);
		if (vp->first == _serialForObj) {
			DEBUR("\tGot match for "<<vp->first);
			_mapLock.unlock();
			return true;
		}
		vp++;
	}
	_mapLock.unlock();
	DEBUR("Get init let go of dat lock");
	return false;
}
ViewPort RealSense::getViewPort() {
	return getViewPort(_serialForObj);
}

ViewPort RealSense::getViewPort(rs2_serial &) {
	//DONT knwo if this works please dont use it 
	_mapLock.lock_shared();
	auto vp = _viewPorts.begin();
	while (vp != _viewPorts.end()) {
		if (vp->first == _serialForObj) {
			_mapLock.unlock();
			return vp->second;
		}
		vp++;
	}
	_mapLock.unlock();

}
std::vector<std::string> RealSense::getDeviceVector() { 
	std::vector<std::string> lista;
	_mapLock.lock_shared();
	auto vp = _viewPorts.begin();
	while (vp != _viewPorts.end()) {
		lista.push_back(vp->first);
		vp++;
	}
	_mapLock.unlock();
	return lista; 
};
