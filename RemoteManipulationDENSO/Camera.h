#pragma once
#include "modules/realsense/realsense.h"
#include "modules/ITRI_Vision/ITRI_Vision.h"
#include "imgproc.hpp"

#include "Util.h"
class Camera
{
public:
	// Need the realsense as a parameter
	Camera();
	~Camera();
	void takeImage();
	void preprocess();
	void placeContours();
	void showImages();
	cv::Mat getImage();

private:
	
	//RealSense rs;
	ITRI_Vision_worker vision;
	cv::Mat image, depth_image, infrared_image;
	cv::Mat croppedImage, preprocessed;
	std::vector<std::vector<cv::Point>> contours;
	std::vector<cv::Point> largestPoint;
	std::vector<cv::Vec4i> hierarchy;
	std::vector<cv::Point2f> cont_points;
	cv::RotatedRect rect;
	cv::Mat gray, thresh;
	pcl::PointCloud<pcl::PointXYZRGB>::Ptr layers;
};

