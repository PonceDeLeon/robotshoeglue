#include "Camera.h"
// Initialize the Realsense

Camera::Camera(/*RealSense& realsense*/)
{
	/*
	rs.initialize();
	if (rs.getInitStatus()) {
		printf("Realsense camera initiated.\n");
	}
	else {
		printf("Realsense NOT initiated.\n");
	}
	*/
}
Camera::~Camera() {
}
// Use either RealSense or static image 
void Camera::takeImage() {
	//printf("Taking image with RealSense...\n");
	//rs.run(image, depth_image, infrared_image, layers);
	image = cv::imread("rs_shoe_back_Color.PNG", CV_LOAD_IMAGE_GRAYSCALE);
	//cv::imshow("Are we capturing", this->image);
	//cv::waitKey(0);
}
void Camera::preprocess() {	
	if (image.empty()) {
		printf("Image is emtpy after resizing!\n");
	}
	cropImageConveyor(image, 0.35, 1.0);
	int c1 = 200, c2 = 240;
	
	// Sharpen the image 
	vision.Pre_precessing(image, c1, c2);
	cv::Mat imgNoBackg = vision.Background_remove(image);
}
void Camera::placeContours() {
	float one = 1.0f;
	//cv::findContours(image, contours, hierarchy,CV_RETR_CCOMP, CV_CHAIN_APPROX_SIMPLE);
	vision.Find_object_contour(image, cont_points, rect, one);
	std::cout << "Image size: " << image.size() << "\tcont points: " 
		<< cont_points.size() << std::endl;
	cv::drawContours(image, contours, vision.getLargestIndex(), cv::Scalar(0, 0, 0), 1, 3, vision.getHierarchy());
}

cv::Mat Camera::getImage() {
	return image;
}
