﻿#include <stdlib.h>
#include <conio.h>
#include <string.h>
#include <math.h>
#include <chrono>


#include "modules/ITRI_Vision/ITRI_Vision.h"
#include "modules/realsense/realsense.h"
#include "opencv2/opencv.hpp"
#include "opencv2/core.hpp"
#include "opencv2/opencv.hpp"
#include "opencv2/imgcodecs.hpp"
#include "opencv2/imgproc.hpp"
#include "opencv2/highgui.hpp"
#include "pcl/point_types.h"

#include "Arm.h"
#include "ReadDataTxtArray.h"
#include "Util.h"
#include "Camera.h"
#define NUM_POINTS 30

// crop the sides of image and only leave conveyor belt
void main(int argc, char** argv)
{
	
	//rs2_serial serial_num = "819312072550";
	//RealSense rs(serial_num);
	ITRI_Vision_worker vision;
	Robot::RobotArm arm;
	//Camera cam(rs); Use this version when realsense connected
	Camera cam;

	arm.connectRobot();
	//
	//arm.waitUntilRobotInput();
	//cam.takeImage();
	//cam.preprocess();
	//cam.placeContours();


	//printf("Sole detected on conveyor.\n");
	//=============================================/
	//===== Getting and processing the image =====/
	//=============================================/

	// get the color image
	//=============================================/
	//===== Sending the coordinates to robot =====/
	//=============================================/
	//double x = 0.0; double y = 0.0; double z = 90.0f; 
	//double rx = 0.0f; double ry = 90.0f; double rz = 0.0f;
	Robot::Position pos;
	pos.x = 0.; pos.y = 0, pos.z = 90;
	pos.rx = 0; pos.ry = 90, pos.rz = 0;
	
	int i = 1, inc = 0;
	arm.prepareRobot(4);
	while (i <= NUM_POINTS) {

		std::chrono::high_resolution_clock::time_point t1 =
			std::chrono::high_resolution_clock::now();
			arm.sendIntCoords(pos.x, pos.y, pos.z, pos.rx, pos.ry, pos.rz); 
			Sleep(300); 
		
		
		std::chrono::high_resolution_clock::time_point t2 =
			std::chrono::high_resolution_clock::now();

		std::chrono::duration<double, std::milli> fp_ms = t2 - t1;
		auto int_ms = std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1);
		std::cout << "\tonly SENDING takes " << fp_ms.count() << std::endl;

		//	printf("\tturn %d: %f, %f, %f, %f, %f, %f\n", 
		//		i, x, y,z, rx, ry, rz );
		
		//system("pause");
		
		if (i > 2) {
			pos.x += 1;
		}
		// Timing the function
		i++;

	}
	//arm.MovP(x, y, z, rx, ry, rz);
	//arm.prepareRobot(11);
	//arm.setOutput(1);
	//arm.setTool(1, 2, 3, 4, 5, 6);
	// (Rough) Steps of the pipeline:
	// 1. Take the image with the realsense
	// 2. Use the img proc to find the edges x
	// 3. Map the points of the coordinates from the img->robot coords.
	// 4. Have the robot move to the mapped coordinates we found.
	//Change this to get image from realsense rather than local
	// Can't mix jpeg and png

	// Use PCL instead of opencv points b/c realsense lib uses it
	//std::vector<std::vector<pcl::PointXYZ>> conts3d;
	//projectContoursTo3D(rs, conts3d, contours, hierarchy);
	
	
	return;
}
